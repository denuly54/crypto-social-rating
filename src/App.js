import React from "react";
import './App.css';
import AuthStorage from "./storages/auth/AuthStorage";
import BaseNavigateRouter from "./BaseNavigateRouter";
import {ethers} from "ethers";

const web3Provider = new ethers.providers.Web3Provider(window.ethereum);
const authStorage = new AuthStorage(localStorage, web3Provider);

const App = () => {
  return (
    <div className="App">
      <header className="App-header">
      </header>
      <BaseNavigateRouter authStorage={authStorage}/>
      <footer className="App-footer">
        <p>some footer</p>
      </footer>
    </div>
  );
}

export default App;
