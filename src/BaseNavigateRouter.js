import React from "react"
import {BrowserRouter, Navigate, Route, Routes} from "react-router-dom";
import Auth from "./pages/Auth/intex";
import Home from "./pages/Home";
import NotFound from "./pages/NotFound/intex";


const BaseNavigateRouter = ({authStorage}) => {
  const isAuthorized = authStorage.getAuth() != null;

  if (!isAuthorized) {
    return (
      <BrowserRouter>
        <Routes>
          <Route path="auth" element={<Auth authStorage={authStorage}/>}/>
          <Route path="*" element={<Navigate to="auth" replace/>}/>
        </Routes>
      </BrowserRouter>
    )
  }

  return (
    <BrowserRouter>
      <Routes>
        <Route path="" element={<Home/>}/>
        <Route path="*" element={<NotFound title={"404"}/>}/>
      </Routes>
    </BrowserRouter>
  )
}

export default BaseNavigateRouter
