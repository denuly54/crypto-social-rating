import { render, screen } from '@testing-library/react';
import BaseNavigateRouter from "./BaseNavigateRouter";
import {BrowserRouter} from "react-router-dom";

test('user is not authorized', () => {
  let authProviderMock = {};

  authProviderMock.getAuth = () => {
    return null
  }


  render(<BrowserRouter><BaseNavigateRouter authStorage={authProviderMock}/> </BrowserRouter>);

  const linkElement = screen.getByText(/authorize/i);

  expect(linkElement).toBeInTheDocument();
});

test('user is authorized', () => {
  let authProviderMock = {};

  authProviderMock.getAuth = () => {
    return "some auth str"
  }


  render(<BaseNavigateRouter authStorage={authProviderMock}/>);

  const linkElement = screen.getByText(/Home page/i);

  expect(linkElement).toBeInTheDocument();
});

