import React, {useState} from "react";
import "./style.css"

const PrimaryButton = ({buttonText, onClick}) => {
  const [isLoad, setIsLoad] = useState(false);

  return (
    <div className="PrimaryButton" onClick={() => onClick(setIsLoad)}>
      {
        isLoad ? (
          <p>Loading...</p>
        ) : (
          <p>{buttonText}</p>
        )
      }
    </div>
  )
}

export default PrimaryButton;
