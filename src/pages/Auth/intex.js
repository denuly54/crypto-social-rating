import React from "react";
import "./style.css"
import PrimaryButton from "../../components/PrimaryButton/intex";

const Auth = ({authStorage}) => {
  const sleep = ms => new Promise(r => setTimeout(r, ms));

  const authUser = async (setLoading) => {
    setLoading(true)

    await sleep(5*1_000)

    await authStorage.authorize()

    await sleep(5*1_000)

    setLoading(false)
  }

  return (
    <div className="AuthPage">
      <p>You need to auth</p>

      <PrimaryButton buttonText={"authorize"} onClick={authUser}/>
    </div>
  )
}

export default Auth;
