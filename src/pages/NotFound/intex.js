import React from "react";
import "./style.css"
import PrimaryButton from "../../components/PrimaryButton/intex";
import {useNavigate} from "react-router-dom";

const NotFound = ({title}) => {
  let navigate = useNavigate()

  return (<div className="NotFound">
    <p>{title}</p>

    <div className="PrimaryButtonContainer">
      <PrimaryButton buttonText={"go home"} onClick={() => {
        navigate("")
      }}/>
    </div>
  </div>)
}

export default NotFound;
