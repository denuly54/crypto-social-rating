class AuthStorage {

  constructor(ls, web3Provider) {
    this.ls = ls
    this.web3Provider = web3Provider
    this.userStorageKey = "user_wallet_address"
  }

  async authorize() {
    await this.web3Provider.send('eth_requestAccounts', []);

    const signer = this.web3Provider.getSigner()

    const address = await signer.getAddress();

    this.ls.setItem(this.userStorageKey, address)
  }

  getAuth() {
    return this.ls.getItem(this.userStorageKey)
  }

  unAuth() {
    this.ls.removeItem(this.userStorageKey)
  }
}

export default AuthStorage